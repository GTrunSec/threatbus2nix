import (
	"encoding/yaml"
)
job: "threatbus": {
	datacenters: ["dc1"]
	type: "batch"
	group: "threatbus": {
		task: "threatbus": {
			driver: "nix"

			resources: {
				cpu:    1000
				memory: 1024
			}

			template: {
				destination: "test.yaml"
				data: "./config.plugins.yaml"
			}

			kill_timeout: "100s"

			config: {
				packages: [
					"github:nixos/nixpkgs/nixos-21.05#bash",
					"github:nixos/nixpkgs/nixos-21.05#coreutils",
					"github:GTrunSec/threatbus2nix#threatbus-latest",
				]
				command: ["/bin/bash", "-c", "sleep 6000"]
			}
		}
	}
}

{
  description = "The missing link to connect open-source threat intelligence tools.";
  nixConfig.extra-substituters = "https://zeek.cachix.org";
  nixConfig.extra-trusted-public-keys = "zeek.cachix.org-1:Jv0hB/P5eF7RQUZgSQiVqzqzgweP29YIwpSiukGlDWQ=";
  nixConfig = {
    flake-registry = "https://github.com/hardenedlinux/flake-registry/raw/main/flake-registry.json";
  };

  inputs = {
    # follows inputs
    nixos.url = "github:NixOS/nixpkgs/nixos-21.05-small";
    flake-compat.flake = false;
    bud = { url = "github:gtrunsec/bud/extend"; };
  };

  outputs =
    inputs @
    { self
    , nixpkgs
    , nixos
    , flake-utils
    , flake-compat
    , devshell
    , vast2nix
    , nixpkgs-hardenedlinux
    , zeek2nix
    , bud
    }:
    rec {
      nixpkgs.config.packageOverrides = pkgs: { } // self.packages."${pkgs.stdenv.hostPlatform.system}";
      nixosModules = {
        threatbus = {
          imports = [
            ./modules/threatbus.nix
          ];
          inherit nixpkgs;
        };
        threatbus-vast = {
          imports = [
            ./modules/threatbus-vast.nix
          ];
          inherit nixpkgs;
        };
      };
    }
    // (
      flake-utils.lib.eachDefaultSystem (
        system: let
          overlay = import ./nix/overlay.nix { inherit inputs; };
          pkgs = inputs.nixpkgs.legacyPackages."${system}".appendOverlays [
            overlay
            (
              final: prev: {
                inherit
                  (inputs.nixpkgs-hardenedlinux.packages."${system}")
                  broker
                  ;
                inherit (inputs.zeek2nix.packages."${system}") zeek-release;
                inherit
                  (inputs.vast2nix.packages."${system}")
                  vast-release
                  pyvast
                  ;
              }
            )
          ];
        in
          rec {
            devShell = import ./shell/devshell.nix { inherit inputs self pkgs; };

            apps = import ./shell/app.nix { inherit inputs pkgs; };

            packages =
              flake-utils.lib.flattenTree {
                inherit
                  (pkgs)
                  threatbus
                  threatbus-latest
                  threatbus-zeek
                  vast-threatbus-latest
                  vast-threatbus
                  vast-release
                  ;
              }
              // pkgs.lib.optionalAttrs pkgs.stdenv.isLinux {
                inherit (pkgs) tenzir-nix-docker;
                inherit
                  (pkgs.threatbus-vm-tests)
                  threatbus-vm-systemd
                  threatbus-vast-vm-systemd
                  ;
              };

            hydraJobs = {
              inherit packages;
            };
            defaultPackage = pkgs.threatbus;
          }
      )
    )
    // {
      budModules = { };
    };
}

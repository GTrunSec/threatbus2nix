{ config
, lib
, pkgs
, ...
}:
with lib;
let
  cfg = config.services.threatbus-vast;
  format = pkgs.formats.yaml { };
  configFile =
    let
      toJsonFile = pkgs.runCommand "extraConfigFile.json" { preferLocalBuild = true; } ''
        ${pkgs.remarshal}/bin/yaml2json  -i ${cfg.extraConfigFile} -o $out
      '';
    in
      format.generate "threatbus.yaml" (
        if cfg.extraConfigFile == null
        then cfg.settings
        else (lib.recursiveUpdate cfg.settings (lib.importJSON toJsonFile))
      );
in
{
  options = {
    services.threatbus-vast = {
      enable = mkEnableOption "enable threatbus-vast";

      extraConfigFile = mkOption {
        type = types.nullOr types.path;
        default = null;
        example = lib.literalExpression ''
          extraConfigFile = ./threatbus-vast.yaml;
        '';
        description = "Load the yaml config file";
      };

      dataDir = mkOption {
        type = types.path;
        default = "/var/lib/threatbus-vast";
        description = ''
          Data directory for threatbus vast
        '';
      };

      package = mkOption {
        type = types.package;
        default = pkgs.vast-threatbus;
        description = "The threatbus-vast package.";
      };

      settings = mkOption {
        type = lib.types.submodule {
          options = {
            vast_binary = mkOption {
              type = types.str;
              default = "${pkgs.vast-release}/bin/vast";
            };
          };
        };
        default = { };
        example = lib.literalExpression ''
          {  }
        '';
        description = ''
          Configuration for threatbus vast. See
          https://github.com/threatbus/tree/threatbus.yaml.example for supported
          options.
        '';
      };
    };
  };

  config = mkIf cfg.enable {
    systemd.services.threatbus-vast = {
      enable = true;
      description = "Vast::The missing link to connect open-source threat intelligence tools.";
      wantedBy = [ "multi-user.target" ];

      after = [
        "network-online.target"
        "vast.service"
        "threatbus.service"
      ];

      environment = {
        VAST_THREATBUS = "${cfg.dataDir}";
      };

      serviceConfig = {
        Restart = "always";
        RestartSec = "10";
        Type = "simple";
        ExecStart = "${cfg.package}/bin/vast-threatbus --config=${configFile}";
        ExecReload = "${pkgs.coreutils}/bin/kill -HUP $MAINPID";
        User = "threatbus";
        WorkingDirectory = "${cfg.dataDir}";
        ReadWritePaths = "${cfg.dataDir}";
        RuntimeDirectory = "threatbus-vast";
        CacheDirectory = "threatbus-vast";
        StateDirectory = "threatbus-vast";
        SyslogIdentifier = "threatbus-vast";
        PrivateUsers = true;
        ProtectSystem = "strict";
        DynamicUser = true;
        PrivateTmp = true;
        ProtectHome = true;
        PrivateDevices = true;
        ProtectKernelTunables = true;
        ProtectControlGroups = true;
        ProtectKernelModules = true;
        ProtectKernelLogs = true;
      };
    };
  };
}

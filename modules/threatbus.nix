{ config
, lib
, pkgs
, ...
}:
with lib;
let
  cfg = config.services.threatbus;
  format = pkgs.formats.yaml { };
  configFile =
    let
      toJsonFile = pkgs.runCommand "extraConfigFile.json" { preferLocalBuild = true; } ''
        ${pkgs.remarshal}/bin/yaml2json  -i ${cfg.extraConfigFile} -o $out
      '';
    in
      format.generate "threatbus.yaml" (
        if cfg.extraConfigFile == null
        then cfg.settings
        else (lib.recursiveUpdate cfg.settings (lib.importJSON toJsonFile))
      );
in
{
  options = {
    services.threatbus = {
      enable = mkEnableOption "enable threatbus";

      extraConfigFile = mkOption {
        type = types.nullOr types.path;
        default = null;
        example = lib.literalExpression ''
          extraConfigFile = ./threatbus.yaml;
        '';
        description = "Load the yaml config file";
      };

      dataDir = mkOption {
        type = types.path;
        default = "/var/lib/threatbus";
        description = ''
          Data directory for threatbus
        '';
      };

      package = mkOption {
        type = types.package;
        default = pkgs.threatbus-latest;
        description = "The threatbus package.";
      };

      settings = mkOption {
        type = lib.types.submodule {
          options = { };
        };
        default = { };
        example = lib.literalExpression ''
          {  }
        '';
        description = ''
          Configuration for Threatbus. See
          https://github.com/threatbus/tree/threatbus.yaml.example for supported
          options.
        '';
      };
    };
  };

  config = mkIf cfg.enable {
    systemd.services.threatbus = {
      description = "The missing link to connect open-source threat intelligence tools.";
      wantedBy = [ "multi-user.target" ];

      after = [
        "network-online.target"
      ];

      environment = {
        THREATBUSDIR = "${cfg.dataDir}";
      };

      serviceConfig = {
        Restart = "always";
        RestartSec = "10";
        ExecStart = "${cfg.package}/bin/threatbus --config=${configFile}";
        ExecReload = "${pkgs.coreutils}/bin/kill -HUP $MAINPID";
        Type = "simple";
        WorkingDirectory = "${cfg.dataDir}";
        ReadWritePaths = "${cfg.dataDir}";
        RuntimeDirectory = "threatbus";
        CacheDirectory = "threatbus";
        StateDirectory = "threatbus";
        SyslogIdentifier = "threatbus";
        PrivateUsers = true;
        ProtectSystem = "strict";
        DynamicUser = true;
        PrivateTmp = true;
        ProtectHome = true;
        PrivateDevices = true;
        ProtectKernelTunables = true;
        ProtectControlGroups = true;
        ProtectKernelModules = true;
        ProtectKernelLogs = true;
      };
    };
  };
}

#!/usr/bin/env bash
#!/usr/bin/env nix-shell
#!nix-shell -i bash -p cmake gcc openssl caf ncurses5
cd ~/src; git clone https://github.com/zeek/broker.git; cd broker
git submodule update --init --recursive
./configure --prefix=$(pwd)/build/install_dir --python-prefix=$(python -c 'import sys; print(sys.exec_prefix)')
make -j && make install
python -c 'import broker; print(broker.__file__)'

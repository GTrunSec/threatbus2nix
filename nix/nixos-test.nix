{ makeTest ? import (inputs.nixos + "/nixos/tests/make-test-python.nix" )
, pkgs
, inputs
}:
{
  threatbus-vm-systemd = makeTest {
    name = "threatbus-systemd";
    machine =
      { config
      , pkgs
      , ...
      }:
      {
        imports = [
          inputs.self.nixosModules.threatbus
          inputs.self.nixosModules.threatbus-vast
        ];

        virtualisation = {
          memorySize = 4046;
          cores = 4;
        };

        services.threatbus = {
          enable = true;
          extraConfigFile = ../conf/config.example.yaml;
        };
      };
    testScript = ''
      start_all()
      machine.wait_for_unit("multi-user.target")
      machine.wait_for_unit("threatbus.service")
      with subtest("test threatbus ZeroMQ Ports"):
           machine.wait_for_open_port(13379)#zeroMQ-app
           machine.wait_for_open_port(13372)#zeroMQ-app
           machine.wait_for_open_port(13373)#zeroMQ-app

      #zeek broker to threatbus TEST
      result = os.linesep + machine.succeed("${pkgs.zeek-release}/bin/zeek ${
      ../conf/zeek-broker.zeek
    }")
      print("Broker:", result)
    '';
  } {
    inherit pkgs;
    inherit (pkgs) system;
  };
  threatbus-vast-vm-systemd = makeTest {
    name = "threatbus-vast-vm-systemd";
    machine =
      { config
      , pkgs
      , ...
      }:
      {
        imports = [
          inputs.self.nixosModules.threatbus-vast
          inputs.self.nixosModules.threatbus
          inputs.vast2nix.nixosModules.vast
        ];

        virtualisation.memorySize = 2046;

        services.vast = {
          enable = true;
          package = pkgs.vast-release;
          extraConfigFile = ../conf/vast.yaml;
        };

        services.threatbus = {
          enable = true;
          extraConfigFile = ../conf/config.example.yaml;
        };

        services.threatbus-vast = {
          enable = true;
          extraConfigFile = ../conf/config.vast.example.yaml;
        };
      };
    testScript = ''
      start_all()
      machine.wait_for_unit("multi-user.target")
      machine.wait_for_unit("threatbus.service")
      with subtest("test threatbus ZeroMQ Ports"):
           machine.wait_for_open_port(13379)#zeroMQ-app
           machine.wait_for_open_port(13372)#zeroMQ-app
           #machine.wait_for_open_port(13373)#zeroMQ-app
      machine.wait_for_unit("vast.service")
      machine.wait_for_unit("threatbus-vast.service")
    '';
  } {
    inherit pkgs;
    inherit (pkgs) system;
  };
}

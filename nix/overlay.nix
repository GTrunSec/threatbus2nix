{ inputs }:
final: prev: let
  plugin-version = "2022.1.27";
in
  {
    threatbus-sources = prev.callPackage ./_sources/generated.nix { };

    threatbus-vm-tests = prev.callPackage ./nixos-test.nix {
      inherit inputs;
    };

    tenzir-nix-docker =
      with final;
      dockerTools.buildImage {
        name = "tenzir-nix-docker";
        tag = "latest";
        contents = [
          threatbus
          vast-release
        ];
        runAsRoot = ''
          #!${pkgs.runtimeShell}
          mkdir -p /var/lib/tenzir
        '';
        config = {
          Cmd = [ "/bin/threastbus" "/bin/vast" ];
          WorkingDir = "/var/lib/tenzir";
          Volumes = { "/var/lib/tenzir" = { }; };
        };
      };

    threatbus-zeek =
      with final;
      stdenv.mkDerivation rec {
        inherit (final.threatbus-sources.threatbus-latest) src pname version;
        name = "threatbus-zeek";
        phases = [ "installPhase" ];
        buildInputs = [ ];
        installPhase = ''
          runHook preInstall
          mkdir -p $out/scripts
          cp -r $src/apps/zeek/* $out/scripts
          runHook postInstall
        '';
      };

    threatbus-latest =
      with final;
      (
        final.threatbus.overrideAttrs (
          old: rec {
            inherit
              (final.threatbus-sources.threatbus-latest)
              src
              pname
              version
              ;
            propagatedBuildInputs =
              with final.python3Packages;
              [
                stix2
                dynaconf
                coloredlogs
                python-dateutil
                black
                (
                  pluggy.overrideAttrs (
                    old: {
                      inherit
                        (final.threatbus-sources.pluggy)
                        pname
                        src
                        version
                        ;
                    }
                  )
                )
                (
                  threatbus-zeek-plugin.overrideAttrs (
                    old: {
                      inherit
                        (final.threatbus-sources.threatbus-latest)
                        src
                        version
                        ;
                      plugin-version = version;
                    }
                  )
                )
                (
                  threatbus-inmem.overrideAttrs (
                    old: {
                      inherit
                        (final.threatbus-sources.threatbus-latest)
                        src
                        version
                        ;
                      plugin-version = version;
                    }
                  )
                )
                (
                  threatbus-file-benchmark.overrideAttrs (
                    old: {
                      inherit
                        (final.threatbus-sources.threatbus-latest)
                        src
                        version
                        ;
                      plugin-version = version;
                    }
                  )
                )
                (
                  threatbus-zmq-app.overrideAttrs (old: { plugin-version = version; })
                )
                final.broker
              ];
          }
        )
      );

    vast-threatbus-latest =
      with final;
      (
        final.vast-threatbus.overrideAttrs (
          old: {
            inherit (final.threatbus-sources.threatbus-latest) src pname;
            propagatedBuildInputs =
              with final.python3Packages;
              [
                stix2-patterns
                dynaconf
                black
                pyzmq
                coloredlogs
                pyvast
                threatbus-latest
              ];
          }
        )
      );

    vast-threatbus =
      with final;
      (
        python3Packages.buildPythonPackage rec {
          pname = "vast-threatbus";
          inherit (final.threatbus-sources.threatbus-release) src version;
          preConfigure = ''
            cd apps/vast
          '';

          doCheck = false;

          propagatedBuildInputs =
            with python3Packages;
            [
              stix2
              stix2-patterns
              confuse
              black
              pyzmq
              threatbus-zmq-app
              coloredlogs
              pyvast
              threatbus
            ];

          postPatch = ''
            substituteInPlace apps/vast/setup.py \
            --replace "threatbus >= ${version}" "" \
            --replace "threatbus-zmq-app >= 2020.12.16" ""
          '';

          meta =
            with lib;
            {
              description = "The missing link to connect open-source threat intelligence tools.";
              homepage = "https://github.com/tenzir/threatbus";
              platforms = platforms.unix;
              license = "BSD-3-Clause";
            };
        }
      );

    dynaconf =
      with final;
      (
        python3Packages.buildPythonPackage rec {
          inherit (final.threatbus-sources.dynaconf) src version pname;
          doCheck = false;
          propagatedBuildInputs =
            with python3Packages;
            [
              setuptools
            ];
        }
      );

    stix2-patterns =
      with final;
      (
        python3Packages.buildPythonPackage rec {
          inherit (final.threatbus-sources.stix2-patterns) src version pname;
          doCheck = false;
          propagatedBuildInputs =
            with python3Packages;
            [
              antlr4-python3-runtime
              six
            ];
        }
      );

    stix2 =
      with final;
      (
        python3Packages.buildPythonPackage rec {
          inherit (final.threatbus-sources.stix2) src version pname;
          doCheck = false;
          propagatedBuildInputs =
            with python3Packages;
            [
              stix2-patterns
              requests
              pytz
              simplejson
            ];
        }
      );

    threatbus-zmq-app =
      with final;
      (
        python3Packages.buildPythonPackage rec {
          pname = "threatbus-zmq-app";
          inherit (final.threatbus-sources.threatbus-release) src version;

          preConfigure = ''
            cd plugins/apps/threatbus_zmq
          '';

          doCheck = false;

          propagatedBuildInputs =
            with python3Packages;
            [
              stix2
              stix2-patterns
              python-dateutil
              pyzmq
            ];

          postPatch = ''
            substituteInPlace plugins/apps/threatbus_zmq/setup.py \
            --replace "threatbus>=${plugin-version}" "" \
            --replace "stix2>=2.1,<3.0" ""
          '';

          meta =
            with lib;
            {
              description = "The missing link to connect open-source threat intelligence tools.";
              homepage = "https://github.com/tenzir/threatbus";
              platforms = platforms.unix;
              license = "BSD-3-Clause";
            };
        }
      );

    threatbus-zeek-plugin =
      with final;
      python3Packages.buildPythonPackage rec {
        pname = "threatbus_zeek";
        inherit (final.threatbus-sources.threatbus-release) src version;

        preConfigure = ''
          cd plugins/apps/threatbus_zeek
        '';

        doCheck = false;

        propagatedBuildInputs =
          with python3Packages;
          [
            stix2
            stix2-patterns
          ];

        postPatch = ''
          substituteInPlace plugins/apps/threatbus_zeek/setup.py \
          --replace "threatbus >= ${plugin-version}" ""
        '';

        meta =
          with lib;
          {
            description = "The missing link to connect open-source threat intelligence tools.";
            homepage = "https://github.com/tenzir/threatbus";
            platforms = platforms.unix;
            license = "BSD-3-Clause";
            # BSD 3-Clause variant
          };
      };

    threatbus-inmem =
      with final;
      python3Packages.buildPythonPackage rec {
        pname = "threatbus_inmem";
        inherit (final.threatbus-sources.threatbus-release) src version;
        preConfigure = ''
          cd plugins/backbones/threatbus_inmem
        '';
        doCheck = false;
        propagatedBuildInputs =
          with python3Packages;
          [
            stix2
            stix2-patterns
          ];

        postPatch = ''
          substituteInPlace plugins/backbones/threatbus_inmem/setup.py \
          --replace "threatbus >= ${plugin-version}" ""
        '';

        meta =
          with lib;
          {
            description = "The missing link to connect open-source threat intelligence tools.";
            homepage = "https://github.com/tenzir/threatbus";
            platforms = platforms.unix;
            license = "BSD-3-Clause";
          };
      };

    threatbus-file-benchmark =
      with final;
      python3Packages.buildPythonPackage rec {
        pname = "threatbus_file_benchmark";
        inherit (final.threatbus-sources.threatbus-release) src version;

        preConfigure = ''
          cd plugins/backbones/file_benchmark
        '';

        doCheck = false;

        propagatedBuildInputs =
          with python3Packages;
          [
            stix2
            stix2-patterns
          ];

        postPatch = ''
          substituteInPlace plugins/backbones/file_benchmark/setup.py \
          --replace "threatbus >= ${plugin-version}" ""
        '';

        meta =
          with lib;
          {
            description = "The missing link to connect open-source threat intelligence tools.";
            homepage = "https://github.com/tenzir/threatbus";
            platforms = platforms.unix;
            license = "BSD-3-Clause";
          };
      };

    threatbus =
      with final;
      python3Packages.buildPythonPackage rec {
        pname = "threatbus";
        inherit (final.threatbus-sources.threatbus-release) src version;

        doCheck = false;

        propagatedBuildInputs =
          with python3Packages;
          [
            stix2
            confuse
            coloredlogs
            python-dateutil
            black
            (
              pluggy.overrideAttrs (
                old: {
                  inherit (final.threatbus-sources.pluggy) pname src version;
                }
              )
            )
            threatbus-zeek-plugin
            threatbus-inmem
            threatbus-file-benchmark
            threatbus-zmq-app
            final.broker
            dynaconf
          ];
        postPatch = ''
          substituteInPlace setup.py \
          --replace "stix2>=2.1,<3.0" "" \
          --replace "pluggy>=1.0" "pluggy"
        '';
        meta =
          with lib;
          {
            description = "The missing link to connect open-source threat intelligence tools.";
            homepage = "https://github.com/tenzir/threatbus";
            platforms = platforms.unix;
            license = "BSD-3-Clause";
          };
      };
  }

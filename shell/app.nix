{ inputs
, pkgs
, ...
}:
with pkgs;
{
  threatbus = {
    type = "app";
    program = "${pkgs.threatbus}/bin/threatbus";
  };
  threatbus-latest = {
    type = "app";
    program = "${pkgs.threatbus-latest}/bin/threatbus";
  };
  vast-threatbus-latest = {
    type = "app";
    program = "${pkgs.vast-threatbus-latest}/bin/vast-threatbus";
  };
  vast-threatbus = {
    type = "app";
    program = "${pkgs.vast-threatbus}/bin/vast-threatbus";
  };
}

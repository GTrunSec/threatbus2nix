{ inputs
, pkgs
, ...
}:
{
  modules = with inputs; [ inputs.bud.devshellModules.bud ];
  exportedModules = [ (inputs.devshell.lib.importTOML ./commands.toml) ];
}

{ inputs
, self
, pkgs
}:
let
  eval = import "${inputs.devshell}/modules" pkgs;
  configuration = {
    name = pkgs.lib.mkDefault "devshell";
    imports =
      let
        devshell = import ./. {
          inherit inputs pkgs;
        };
      in
        devshell.modules ++ devshell.exportedModules;
  };
in
(
  eval {
    inherit configuration;
    extraSpecialArgs = { inherit self inputs; };
  }
)
.shell

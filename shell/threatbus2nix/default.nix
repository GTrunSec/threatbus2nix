{ pkgs
, lib
, budUtils
, inputs
, ...
}:
{
  bud.cmds =
    with pkgs;
    {
      zeek = {
        writer = budUtils.writeBashWithPaths [ zeek-release threatbus nmap gnugrep procps ];
        synopsis = "zeek [config.yml threatbus.zeek ]";
        help = "test your zeek script to threatbus";
        script = ./zeek.bash;
        extraScript = ''
          export PRJ=${../.}
          export ZEEK_SCRIPT=${pkgs.threatbus-zeek}
        '';
      };
      nomad = {
        writer = budUtils.writeBashWithPaths [ nomad coreutils ];
        synopsis = "nomad";
        help = "deploy threatbus to nomad";
        script = ./nomad.bash;
      };
    };
}

#!/usr/bin/env bash
#
if [[ ! -z $(nmap -p 47761 127.0.0.1 | grep "open") ]]; then
    pkill -f threatbus
fi

if [[ ! -z "$1" ]]; then
    threatbus -c "$1" &
else
    threatbus -c $FLAKEROOT/conf/config.example.yaml &
fi

if [ [ ! -z "$2" ]]; then
    #zeek $FLAKEROOT/conf/threatbus.zeek
    zeek $2
else
    zeek $ZEEK_SCRIPT/scripts/threatbus.zeek
fi
